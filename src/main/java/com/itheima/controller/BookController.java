package com.itheima.controller;

import com.itheima.pojo.Book;
import com.itheima.pojo.PageBean;
import com.itheima.pojo.Result;
import com.itheima.service.BookService;
import org.apache.ibatis.annotations.Insert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class BookController {
    @Autowired
    private BookService bookService;

    @GetMapping("/books")
        public Result select(@RequestParam(defaultValue = "1") Integer page,@RequestParam(defaultValue = "10") Integer pageSize,String name){
            PageBean pageBean=bookService.select(page,pageSize,name);
            return Result.success(pageBean);

        }

        @PostMapping("/books")
        public Result add(@RequestBody Book book){
            bookService.add(book);
            return Result.success();
        }

        @PutMapping("/books/{id}")
        public Result getById(@PathVariable Integer id){
        bookService.getById(id);
        return Result.success();
        }

        @PutMapping("/books")

    public Result update(@RequestBody Book book){

        bookService.update(book);
            return Result.success();
        }

        @DeleteMapping("/books/{id}")
        public Result delete(@PathVariable Integer id ){
            bookService.delete(id);
            return Result.success();
        }

}
