package com.itheima.mapper;

import com.itheima.pojo.Book;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface BookMapper {
    List<Book> select(String name);

    @Insert("insert into t_book(type, name, description) values(#{type},#{name},#{description})")
    void add(Book book);

    @Select("select * from t_book where id =#{id};")
    void getById(Integer id);

    void update(Book book);

    @Delete("delete from t_book where id=#{id}")
    void deleteById(Integer id);
}
