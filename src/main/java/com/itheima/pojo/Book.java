package com.itheima.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Book {
    private Integer id;         //书籍id
    private String type;        //书籍类型
    private String name;        //书籍名称
    private String description; //书籍描述
}
