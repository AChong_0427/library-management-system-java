package com.itheima.service;

import com.itheima.pojo.Book;
import com.itheima.pojo.PageBean;
import org.apache.ibatis.annotations.Insert;

public interface BookService {
    PageBean select(Integer page, Integer pageSize, String name);

    void add(Book book);

    void getById(Integer id);

    void update(Book book);

    void delete(Integer id);
}
