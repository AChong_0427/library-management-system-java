package com.itheima.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itheima.mapper.BookMapper;
import com.itheima.pojo.Book;
import com.itheima.pojo.PageBean;
import com.itheima.service.BookService;
import org.apache.ibatis.annotations.Insert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookServiceImpl implements BookService {
    @Autowired
    private BookMapper bookMapper;


    @Override
    public PageBean select(Integer page, Integer pageSize, String name) {
        PageHelper.startPage(page,pageSize);



        List<Book> list=bookMapper.select(name);

        Page p=(Page)list;

        return new PageBean(p.getTotal(),p.getResult());
    }

    @Override
    public void add(Book book) {
        bookMapper.add(book);
    }

    @Override
    public void getById(Integer id) {
        bookMapper.getById(id);
    }

    @Override
    public void update(Book book) {
        bookMapper.update(book);
    }

    @Override
    public void delete(Integer id) {
        bookMapper.deleteById(id);
    }
}
